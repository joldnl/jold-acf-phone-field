<?php

/*
    Plugin Name:           ACF: Phone Field
    Description:           Adds a custom <input type="tel"> phone field to ACF with frontend microformatted link.
    Version:               1.0.3
    Plugin URI:            https://bitbucket.org/joldnl/jold-acf-phone-field
    Bitbucket Plugin URI:  https://bitbucket.org/joldnl/jold-acf-phone-field
    Author:                Jurgen Oldenburg
    Author URI:            http://www.jold.nl
    License:               GPLv2 or later
    License URI:           http://www.gnu.org/licenses/gpl-2.0.html
*/




// Include phone field type for ACF5
add_action( 'acf/include_field_types',  'jld_acf_phone_field' );

function jld_acf_phone_field( $version ) {


    class acf_field_phone_field extends acf_field {



        /*
        *  __construct
        *
        *  Main construct function, fired on load
        *
        *  @type    function
        *  @date    06/12/2015
        *  @since   1.0
        *
        *  @param   N/A
        *  @return  N/A
        */
        function __construct() {
            $this->name         = 'phone';                                   // Field name
            $this->label        = __('Phone number', 'acf-phone-field');     // Field label
            $this->category     = 'basic';                                   // Field category
            $this->defaults     = array(
                'multiple'      => 0,
            );

            $this->l10n = array(
                'error'    => __('Error! Please enter a valid phone number', 'acf-phone-field'),
            );

            // Set textdomain
            load_plugin_textdomain( 'acf-phone-field', false, dirname( plugin_basename(__FILE__) ) . '/lang/' );

            // Add actions
            add_action( 'admin_enqueue_scripts',    array( $this, 'jld_acf_phone_field_enqueue' ) );
            add_action( 'acf/include_field_types',  array( $this, 'jld_acf_phone_field' ) );

            // Construct this class in parent class (acf_field)
            parent::__construct();

        }



        /*
        *  render_field_settings
        *
        *  ACF Callback: Render the field settings in the admin screen
        *
        *  @type     function
        *  @date     06/12/2015
        *  @since    1.0
        *
        *  @param    array    $field    The field details array
        *  @return   N/A
        */
        function render_field_settings( $field ) {

            // Set output settings
            acf_render_field_setting( $field, array(
                'label'            => __('Output phone link','acf-phone-field'),
                'instructions'    => __('Enable clickable html phone number link.','acf-phone-field'),
                'type'            => 'radio',
                'name'            => 'output_link',
                'choices'        => array(
                    1 => __("Yes",'acf'),
                    0 => __("No",'acf'),
                ),
                'layout'        =>    'horizontal',
            ));

            // Set default settings
            acf_render_field_setting( $field, array(
                'label'            => __('Default Value','acf'),
                'instructions'    => __('Appears when creating a new post','acf'),
                'type'            => 'text',
                'name'            => 'default_value',
            ));

            // Set placeholder settings
            acf_render_field_setting( $field, array(
                'label'            => __('Placeholder Text','acf'),
                'instructions'    => __('Appears within the input','acf'),
                'type'            => 'text',
                'name'            => 'placeholder',
            ));

        }



        /*
        *  format_value
        *
        *  ACF Callback: Format the frontend output of the phone field
        *
        *  @type     function
        *  @date     06/12/2015
        *  @since    1.0
           *
        *  @param    string    $value      The field value from the database
        *  @param    int       $post_id    The post ip the field value belongs to
        *  @param    array     $field      Field details
        *  @return   mixed                 The formatted field value output
        */
        function format_value( $value, $post_id, $field ) {

            // bail early if no value
            if( empty($value) ) {

                return $value;

            }

            if( $field[ 'output_link' ] ) {

                // Return formatted html
                return '<a href="tel:' . $value . '" itemprop="telephone" class="tel">' . $value . '</a>';

            } else {

                // Return formatted html
                return $value;

            }

        }



        /*
           *  render_field
           *
           *  ACF Callback: Render admin input field
           *
           *  @type      function
           *  @date      06/12/2015
           *  @since     1.0
           *
           *  @param     array    $field    The registered field details
           *  @return    mixed              Rendered html output of the title field
           */
        function render_field( $field ) {
            // echo "<pre>";
            // print_r($field);
            // echo "</pre>";
            echo '<div class="acf-input-prepend dashicons dashicons-phone"></div><div class="acf-input-wrap"><input type="tel" name="' . esc_attr( $field[ 'name' ] ) . '" class="acf-is-prepended" value="' . esc_attr( $field[ 'value' ] ) . '" /></div>';
        }



        // Enqueue css for input type=tel element
        function jld_acf_phone_field_enqueue($hook) {

            $plugin_url = plugin_dir_url( __FILE__ );

            wp_register_style( 'jld_acf-phone-field-css', $plugin_url . 'css/input.css', false, '1.0.0' );
            wp_enqueue_style( 'jld_acf-phone-field-css' );

        }


    }

    new acf_field_phone_field();

}

?>
