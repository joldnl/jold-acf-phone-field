��          �            h     i     �      �     �     �     �  (   �  (        ;     >     P     ]     n     v  �  z  %   [  '   �  O   �  ?   �  	   9     C  4   T  ,   �     �     �     �     �     �     �                                       	                        
                 Appears after the input Appears before the input Appears when creating a new post Appears within the input Append Default Value Enable clickable html phone number link. Error! Please enter a valid phone number No Output phone link Phone number Placeholder Text Prepend Yes Project-Id-Version: ACF Phone Field
POT-Creation-Date: 2015-12-06 18:14+0100
PO-Revision-Date: 2015-12-06 18:15+0100
Last-Translator: JOLD Interactive <info@jold.nl>
Language-Team: JOLD Interactive <info@jold.nl>
Language: Dutch
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.7
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_n;_e;_x
X-Poedit-Basepath: .
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: ..
 Informatie die verschijnt na het veld Informatie die verschijnt voor het veld Vooraf ingevulde waarde die te zien is tijdens het aanmaken van een nieuwe post Informatie die verschijnt in het veld (verdwijnt zodra je typt) Navoegsel Standaard waarde Maak het telefoon nummer klikbaar in in de frontend. Error! Vul aub een geldig telefoon nummer in Nee Klikbare link Telefoon nummer Plaatsvervangende tekst Voorvoegsel Ja 