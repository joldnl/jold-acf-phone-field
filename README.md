# ACF Phone Field

### Description
The ACF Phone Field plugin adds a "Phone Number" field to your Advanced Custom Fields fields list.

### Features
* Add a custom Phone Field to acf fields
* Choose to output the phone number as a clickable tel link
* Microdata and vcard support on clickable link output
* Outputs a mobile and browser compatible <input type="tel" /> field in frontend forms

### Wishlist
* Phone number validation
* Do not activate if ACF 5.x is not activated


## Installation:

Add repository to your local composer file:

    {
        "type": "git",
        "url": "https://joldnl@bitbucket.org/joldnl/jold-acf-phone-field.git"
    }



Add the plugin as a depenency:

    "joldnl/jold-acf-phone-field": "~1.0",


Update composer by running:

    $ composer update.
